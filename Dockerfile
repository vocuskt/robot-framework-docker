FROM ppodgorsek/robot-framework

LABEL org.opencontainers.image.authors="apaulin@cisco.com"


# We use root to have full access
# This is not a good practice, but the container is up only during the test
# Which is supposed to be short
USER root
# Build directories and copy itself
RUN mkdir /test && mkdir results
COPY . /test
WORKDIR /test

RUN pip install robotframework-requests
RUN pip install --upgrade robotframework-lint

# Validate the test with a linter, then run it
ENTRYPOINT rflint /test/* && robot -d /results/ .